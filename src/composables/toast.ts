import { useToast } from 'vue-toast-notification'

export function useToasts() {
  const toast = useToast()

  const showToast = (message: string, type: string = 'success', duration: number = 3000): void => {
    toast.open({
      message: message,
      type: type,
      duration: duration,
      dismissible: true
    })
  }

  return {
    showToast
  }
}
