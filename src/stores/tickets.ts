import { defineStore } from 'pinia'
import { type Ref, ref } from 'vue'
import { useToasts } from '@/composables/toast'
import TicketsService from '@/services/tickets'
import type { Ticket, TicketParams, TicketResponse } from '@/utils/types'

export const useTicketsStore = defineStore('tickets', () => {
  const toasts = useToasts()

  const loading: Ref<boolean> = ref(false)
  const tickets: Ref<Ticket[] | null> = ref(null)

  const createTicket = async (body: TicketParams): Promise<void> => {
    const res: TicketResponse = await TicketsService.createTicket(body)

    if (res && res.data) {
      tickets.value ? tickets.value.push(res.data) : tickets.value = [res.data]
      toasts.showToast('You book ticket successful')
    } else {
      toasts.showToast('Something went wrong', 'error')
    }
  }

  return {
    loading,
    tickets,
    createTicket
  }
})
