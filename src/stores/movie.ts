import { defineStore } from 'pinia'
import { type Ref, ref } from 'vue'
import MoviesService from '@/services/movies'
import type {
  Movie,
  MoviesParams,
  MoviesResponse,
  Places,
  PlacesParams,
  PlacesResponse,
  Session,
  SessionParams,
  SessionResponse
} from '@/utils/types'

export const useMovieStore = defineStore('movie', () => {
  const loading: Ref<boolean> = ref(false)
  const movie: Ref<Movie | null> = ref(null)
  const sessions: Ref<Array<Session> | null> = ref(null)
  const places: Ref<Places | null> = ref(null)

  const getMovie = async (params?: MoviesParams): Promise<void> => {
    const res: MoviesResponse = await MoviesService.getMovies(params)

    if (res.data && res.data.length > 0) {
      movie.value = res.data[0]
    }
  }

  const getMovieSessions = async (params: SessionParams): Promise<void> => {
    const res:SessionResponse = await MoviesService.getSessions(params)

    if (res.data) {
      sessions.value = Object.values(res.data)[0]
    }
  }

  const getSessionPlaces = async (params: PlacesParams): Promise<void> => {
    const res: PlacesResponse = await MoviesService.getPlaces(params)

    if (res.data) {
      places.value = res.data
    }
  }

  return {
    loading,
    movie,
    places,
    sessions,
    getMovie,
    getMovieSessions,
    getSessionPlaces,
  }
})
