import { defineStore } from 'pinia'
import { type Ref, ref } from 'vue'
import MoviesService from '@/services/movies'
import type { Movie, MoviesParams, MoviesResponse } from '@/utils/types'

export const useMoviesStore = defineStore('movies', () => {
  const loading: Ref<boolean> = ref(false)
  const movies: Ref<Movie[] | null> = ref(null)

  const getMovies = async (params?: MoviesParams): Promise<void> => {
    const res: MoviesResponse = await MoviesService.getMovies(params)

    if (res.data) {
      movies.value = res.data
    }
  }

  return {
    loading,
    movies,
    getMovies
  }
})
