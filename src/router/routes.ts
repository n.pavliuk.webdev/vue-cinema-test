import type { RouteRecordRaw } from 'vue-router'

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'movies',
    component: () => import('@/views/HomeView.vue')
  },
  {
    path: '/:id',
    name: 'movie',
    component: () => import('@/views/MovieView.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: () => import('@/views/NotFound.vue')
  }
]
