import type { Genre } from '@/utils/types'

export const genres: Array<Genre> = [
  {id: 0, title: 'Action'},
  {id: 1, title: 'Adventures'},
  {id: 2, title: 'Comedy'},
  {id: 3, title: 'Drama'},
  {id: 4, title: 'Horror'},
  {id: 5, title: 'Westerns'}
]
