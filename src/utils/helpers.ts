import type { Genre } from '@/utils/types'
import { genres } from '@/utils/consts'

export const htmlToText = (html: string): string => {
  html = html.replace(/<style([\s\S]*?)<\/style>/gi, '')
  html = html.replace(/<script([\s\S]*?)<\/script>/gi, '')
  html = html.replace(/<\/div>/ig, '\n')
  html = html.replace(/<\/li>/ig, '\n')
  html = html.replace(/<li>/ig, '  *  ')
  html = html.replace(/<\/ul>/ig, '\n')
  html = html.replace(/<\/p>/ig, '\n')
  html = html.replace(/<br\s*[\/]?>/gi, '\n')
  return  html.replace(/<[^>]+>/ig, '')
}

export const genreName = (index: number):Genre => {
  return genres[index]
}

export const stringToArray = (string: string, separator: string): Array<string> => {
  return string.split(separator)
}
