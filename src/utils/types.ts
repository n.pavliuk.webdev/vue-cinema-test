export interface Genre {
  id: number,
  title: string
}

export interface Movie {
  id: string
  genre: number
  name: string
  image?: string
  description?: string
  additional?: string
}

export interface MoviesParams {
  movie_id?: string
  name?: string
  genres?: []
}

export interface MoviesResponse {
  data: Array<Movie>
}

export interface Session {
  daytime: string
  showdate: string
}

export interface SessionParams {
  movie_id?: string
}

export interface SessionResponse {
  data: Record<string, Array<Session>>
}

export interface Seat {
  seat: number
  is_free: boolean
}

export interface Row {
  row: number
}

export type Places = [Row, Seat[]][]

export interface PlacesParams {
  movie_id: string
  daytime: string
  showdate: string
}

export interface PlacesResponse {
  data: Places
}

export interface Ticket {
  movie_id: number
  daytime: string
  showdate: string
  row: string
  seat: string
  ticketkey: string
}

export interface TicketParams {
  movie_id: string
  daytime: string | null
  showdate: string | null
  row: number | null
  seat: number | null
}

export interface TicketResponse {
  data: Ticket
}

