import api from '@/api/client'
import type { TicketParams, TicketResponse } from '@/utils/types'

export default {
  async createTicket(body: TicketParams): Promise<TicketResponse> {
    return api.post('/bookPlace', body)
      .then(res => res.data)
      .catch(res => console.error(res))
  }
}

