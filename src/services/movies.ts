import api from '@/api/client'
import type {
  MoviesParams,
  MoviesResponse,
  PlacesParams,
  PlacesResponse,
  SessionParams,
  SessionResponse
} from '@/utils/types'

export default {
  async getMovies (params?: MoviesParams): Promise<MoviesResponse> {
    return api.get('/movies', {params})
      .then(res => res.data)
      .catch(res => console.error(res))
  },
  async getSessions  (params: SessionParams): Promise<SessionResponse> {
    return api.get('/movieShows', {params})
      .then(res => res.data)
      .catch(res => console.error(res))
  },
  async getPlaces  (params: PlacesParams): Promise<PlacesResponse> {
    return api.get('/showPlaces', {params})
      .then(res => res.data)
      .catch(res => console.error(res))
  },
}

